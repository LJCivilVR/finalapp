package com.example.luitzen_janbeiboer.finalapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Locale;

@EActivity(R.layout.activity_call)
public class Call extends AppCompatActivity {

    @ViewById
    Button buttoncall;

    @ViewById
    EditText phonenumber;

    @ViewById
    TextView textCall;

    @AfterViews
    protected void setupViews() {

        /** set language device **/
        Log.e("Language", Locale.getDefault().getDisplayLanguage());
        String Language = Locale.getDefault().getDisplayLanguage();

        if (Language.equals("suomi")) {
            textCall.setText("Puhelinnumero:");
            buttoncall.setText("Puhelu");
        } else {
            textCall.setText("Phone number:");
            buttoncall.setText("Call");
        }

        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

    }

    @Click(R.id.buttoncall)
    protected void startCalling() {
        String phoneNumber = phonenumber.getText().toString();
        String callData = new String("tel:") + phoneNumber;
        call(callData);


    }

    protected void call(String callData) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);//dial
        callIntent.setData(Uri.parse(callData));
        Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("0401415086"));
        try {
            startActivity(callIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "yourActivity is not founded", Toast.LENGTH_SHORT).show();
        }
    }

    //monitor phone call activities
    private class PhoneCallListener extends PhoneStateListener {

        private boolean isPhoneCalling = false;

        String LOG_TAG = "LOGGING 123";

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(LOG_TAG, "OFFHOOK");

                isPhoneCalling = true;
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // run when class initial and phone call ended,
                // need detect flag from CALL_STATE_OFFHOOK
                Log.i(LOG_TAG, "IDLE");

                if (isPhoneCalling) {

                    Log.i(LOG_TAG, "restart app");

                    // restart app

                    Intent i = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(
                                    getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                    isPhoneCalling = false;
                }

            }
        }

    }


}
