package com.example.luitzen_janbeiboer.finalapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;


@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView;
//    }


    @Click(R.id.btnFood)
    protected void foodClick() {
        Intent in = new Intent(getApplicationContext(), Food_.class);
        startActivity(in);


    }

    @Click(R.id.btnWeather)
    protected void weatherClick() {
        Intent in = new Intent(getApplicationContext(), Weather_.class);
        startActivity(in);

    }

    @Click(R.id.btnChat)
    protected void chatClick() {
        Intent in = new Intent(getApplicationContext(), Chat.class);
        startActivity(in);

    }

    @Click(R.id.btnCall)
    protected void callClick() {
        Intent in = new Intent(getApplicationContext(), Call_.class);
        startActivity(in);
    }




}
