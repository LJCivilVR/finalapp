package com.example.luitzen_janbeiboer.finalapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

@EActivity(R.layout.activity_food)
public class Food extends AppCompatActivity {


    @ViewById
    TextView dateText;

    @ViewById
    ListView meals;

    @ViewById
    ToggleButton language;

    @ViewById
    Button Yesterday;
    @ViewById
    Button today;
    @ViewById
    Button tomorrow;

    // JSON Node names
    public static final String TAG_NAMELUNCH = "lunchMenu";
    public static final String TAG_NAMEMEAL = "nameMeal";
    public static final String TAG_DIETS = "diets";
    public static final String TAG_DATE = "date";
    public static final String TAG_DIETNAME = "dietName";
    public static final String TAG_DIETLETTER = "dietLetter";


    private static ProgressBar spinner;

    /**
     * set date
     **/
    SimpleDateFormat year = new SimpleDateFormat("yyyy");
    private String yearTime = year.format(new Date());

    SimpleDateFormat month = new SimpleDateFormat("MM");
    private String monthTime = month.format(new Date());

    SimpleDateFormat day = new SimpleDateFormat("dd");
    private String dayTime = day.format(new Date());

    private String languageFinEng = "en";

    private int dag = 0;


    @AfterViews
    protected void setupViews() {

        /** The progressbar **/
        spinner = (ProgressBar) findViewById(R.id.LoadProgress);
        spinner.setVisibility(View.VISIBLE);

        /** set language device **/
        Log.e("Language", Locale.getDefault().getDisplayLanguage());
        String Language = Locale.getDefault().getDisplayLanguage();

        if (Language.equals("suomi")) {
            Log.e("I'm", Language);
            setLanguage("suomi");
        } else {
            setLanguage("english");
        }

    }

    ArrayList<HashMap<String, String>> menuList = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> dietList = new ArrayList<HashMap<String, String>>();


    protected void setLanguage(String TheLanguage) {
        if (TheLanguage.equals("suomi")) {
            languageFinEng = "fi";
            Yesterday.setText("Eilen");
            today.setText("tänään");
            tomorrow.setText("huomenna");
            language.setChecked(true);
            todayDate();
        } else if (!TheLanguage.equals("suomi")) {
            languageFinEng = "en";
            Yesterday.setText("Yesterday");
            today.setText("Today");
            tomorrow.setText("Tomorrow");
            language.setChecked(false);
            todayDate();
        }

    }


    @Click(R.id.Yesterday)
    protected void yesterdayDate() {
        spinner.setVisibility(View.VISIBLE);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, -1); //Goes to previous day
        Date yourDate = cal.getTime();
        dayTime = day.format(yourDate);

        /** give text color **/
        Yesterday.setTextColor(Color.rgb(255, 0, 0));
        today.setTextColor(Color.rgb(0, 0, 0));
        tomorrow.setTextColor(Color.rgb(0, 0, 0));

        menuList.clear();
        createList();
    }

    @Click(R.id.today)
    protected void todayDate() {
        spinner.setVisibility(View.VISIBLE);
        SimpleDateFormat day = new SimpleDateFormat("dd");
        dayTime = day.format(new Date());

        /** give text color **/
        Yesterday.setTextColor(Color.rgb(0, 0, 0));
        today.setTextColor(Color.rgb(255, 0, 0));
        tomorrow.setTextColor(Color.rgb(0, 0, 0));

        menuList.clear();
        createList();
    }

    @Click(R.id.tomorrow)
    protected void tomorrowDate() {
        spinner.setVisibility(View.VISIBLE);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, 1); //Adds a day
        Date yourDate = cal.getTime();
        dayTime = day.format(yourDate);

        /** give text color **/
        Yesterday.setTextColor(Color.rgb(0, 0, 0));
        today.setTextColor(Color.rgb(0, 0, 0));
        tomorrow.setTextColor(Color.rgb(255, 0, 0));

        menuList.clear();
        createList();
    }


    @Click(R.id.language)
    protected void changeLanguage() {
        spinner.setVisibility(View.VISIBLE);
        if (language.isChecked() == true) {
            setLanguage("suomi");
        }

        if (language.isChecked() == false) {
            setLanguage("english");
        }
    }

    /**
     * Read the Menu from API
     **/
    @Background
    protected void createList() {

        String currentDate = yearTime + "-" + monthTime + "-" + dayTime;

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(Food.this);
        String url = "http://www.amica.fi/api/restaurant/menu/day?date=" + currentDate + "&language=" + languageFinEng + "&restaurantPageId=66287";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            /** Get LunchMenu **/
                            JSONObject jObj = new JSONObject(response);

                            /** Don't show the menu when menu is empty **/
                            if (jObj.isNull("LunchMenu")) {

                                String message;
                                String okay;

                                if (languageFinEng.equals("fi")) {
                                    message = "Anteeksi, ei ole valikkoa.\nValitse toinen päivä.";
                                    okay = "okei";
                                } else {
                                    message = "Sorry, there is no menu.\nChoose another day.";
                                    okay = "okay";
                                }

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(Food.this);
                                builder1.setMessage(message);
                                builder1.setCancelable(true);

                                builder1.setPositiveButton(
                                        okay,
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                try {
                                                    showMenu();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                AlertDialog alert11 = builder1.create();
                                alert11.show();
                            }


                            if (!(jObj.isNull("LunchMenu"))) {

                                JSONObject json = (JSONObject) jObj.get("LunchMenu");
                                String date = json.get("Date").toString();

                                JSONArray menus = (JSONArray) json.get("SetMenus");
                                for (int i = 0; i < menus.length(); i++) {
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    JSONObject actor = menus.getJSONObject(i);
                                    String nameLunch = actor.getString("Name");

                                    // adding each child node to HashMap key => value
                                    map.put(TAG_NAMELUNCH, nameLunch);

                                    JSONArray mealMenu = (JSONArray) actor.get("Meals");
                                    for (int k = 0; k < mealMenu.length(); k++) {
                                        JSONObject joop = mealMenu.getJSONObject(k);

                                        String nameMeal = joop.getString("Name");
                                        String diets = joop.getString("Diets");

                                        map.put(TAG_NAMEMEAL, nameMeal);
                                        map.put(TAG_DIETS, diets);
                                    }
                                    map.put(TAG_DATE, date);

                                    menuList.add(map);
                                }

                                /** Get the RequireDietFilters **/
                                JSONArray dietFilters = (JSONArray) jObj.get("RequireDietFilters");
                                Log.e("RequireDietFilters", String.valueOf(dietFilters));

                                for (int v = 0; v < dietFilters.length(); v++) {
                                    HashMap<String, String> mapDiets = new HashMap<String, String>();

                                    JSONObject diet = dietFilters.getJSONObject(v);

                                    String translatedName = diet.getString("TranslatedName");
                                    String translatedDiet = diet.getString("Diet");

                                    mapDiets.put(TAG_DIETNAME, translatedName);
                                    mapDiets.put(TAG_DIETLETTER, translatedDiet);

                                    dietList.add(mapDiets);
                                }
                                Log.e("dietList", String.valueOf(dietList));

                                showMenu();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Something went wrong", error.toString());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    protected void showMenu() throws JSONException {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);

        for (int m = 0; m < menuList.size(); m++) {
            HashMap<String, String> row;
            row = menuList.get(m);

            String date = row.get(TAG_DATE);
            dateText.setText(date);

            String nameMeal = row.get(TAG_NAMEMEAL);
            String lunchMenu = row.get(TAG_NAMELUNCH);

            String menu = lunchMenu + "\n" + nameMeal + "\n";

            adapter.add(menu);
        }
        meals.setAdapter(adapter);

        spinner.setVisibility(View.GONE);
    }


    @ItemClick(R.id.meals)
    void itemClicked(Object item) {
        for (int m = 0; m < menuList.size(); m++) {
            HashMap<String, String> row;
            row = menuList.get(m);
            String nameMeal = row.get(TAG_NAMEMEAL);
            String lunchMenu = row.get(TAG_NAMELUNCH);
            String diets = row.get(TAG_DIETS);

            String menu = lunchMenu + "\n" + nameMeal + "\n";

            String menuItem = item.toString();

            if (menu.equals(menuItem)) {
                ArrayList<String> dietListLetter = new ArrayList<>();
                ArrayList<String> dietListName = new ArrayList<>();

                for (int z = 0; z < (dietList.size() / 3); z++) {
                    HashMap<String, String> rowDiets;
                    rowDiets = dietList.get(z);

                    String dietName = rowDiets.get(TAG_DIETNAME);
                    String dietLetter = rowDiets.get(TAG_DIETLETTER);

                    dietListName.add(dietName);
                    dietListLetter.add(dietLetter);
                }

                Intent in = new Intent(getApplicationContext(), Details_.class);
                in.putExtra(TAG_NAMELUNCH, lunchMenu);
                in.putExtra(TAG_NAMEMEAL, nameMeal);
                in.putExtra(TAG_DIETS, diets);


                /** the RequireDietFilters **/
                in.putExtra("dietListLetter", dietListLetter);
                in.putExtra("dietListName", dietListName);

                startActivity(in);

            }
        }

    }
}
