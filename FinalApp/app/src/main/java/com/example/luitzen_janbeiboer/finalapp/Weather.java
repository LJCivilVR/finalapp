package com.example.luitzen_janbeiboer.finalapp;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Locale;

@EActivity(R.layout.activity_weather)
public class Weather extends AppCompatActivity {

    @ViewById
    Button getWeather;

    @ViewById
    EditText getPlace;

    @ViewById
    TextView giveTemp;

    @ViewById
    ProgressBar spinner;

    @ViewById
    ImageView cloudImage;

    private String thePlace = "";
    private String savePlace;
    private String languageFinEng = "en";
    private String Temp;


    @AfterViews
    protected void setupViews() {
        /**
         * Set latest city back into the EditText and get the weather
         **/
        savePlace = getSavePlace();
        if (savePlace != null && !savePlace.isEmpty()) {
            getPlace.setText(savePlace);
            thePlace = getPlace.getText().toString();

            /** update the weather **/
            findWeather(thePlace);
        }

        /** end **/

        /** set language device **/
        Log.e("Language", Locale.getDefault().getDisplayLanguage());
        String Language = Locale.getDefault().getDisplayLanguage();

        if (Language.equals("suomi")) {
            Log.e("I'm", Language);
            setLanguage("suomi");
        } else {
            setLanguage("english");
        }
    }

    protected void setLanguage(String TheLanguage) {
        if (TheLanguage.equals("suomi")) {
            languageFinEng = "fi";
            getPlace.setHint("Anna kaupungin nimi");
            getWeather.setText("saada sää");
            Temp = "Lämpötila:";

        } else if (!TheLanguage.equals("suomi")) {
            languageFinEng = "en";
            getWeather.setText("Get Weather");
            getPlace.setHint("Give a city name");
            Temp = "Temp:";
        }
        /** The progressbar **/
        spinner.setVisibility(View.GONE);
    }

    /**
     * Save the city
     **/
    private void setSavePlace(String city) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("SaveWeatherCity", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString("lastCity", city);
        editor.apply();
    }

    private String getSavePlace() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("SaveWeatherCity", MODE_PRIVATE);
        String lastPlace = pref.getString("lastCity", null);
        return lastPlace;
    }
    /** end **/


    @Click(R.id.getWeather)
    protected void getThePlaceBtn() {
        thePlace = getPlace.getText().toString();

        if (thePlace.matches("")) {
            Toast.makeText(Weather.this, "You didn't give a city", Toast.LENGTH_LONG).show();
            return;
        }

        Log.e("the place", thePlace);

        spinner.setVisibility(View.VISIBLE);
        setSavePlace(thePlace);
        findWeather(thePlace);
    }


    @Background
    protected void findWeather(String city) {
        String str = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&APPID=65dbec3aae5e5bf9000c7a956c8b76f6";
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(str);
            urlConn = url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
                System.out.println("Line is: " + line);
            }

            setWeatherOnScreen(new JSONObject(stringBuffer.toString()));
        } catch (Exception ex) {
            Log.e("App", "yourDataTask", ex);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @UiThread
    protected void setWeatherOnScreen(JSONObject response) {
        if (response != null) {
            String temperature;
            String showTemp;
            String Search = "main";
            String clouds;
            String weather;

            /** 0 celsius = -273,15 **/
            double KelvinToCels = 273.15;
            double celsius;

            try {
                /** First the temperature **/
                Log.e("App", "Success: " + response.getString(Search));
                temperature = response.getString(Search);

                JSONObject jsonObject = new JSONObject(temperature.toString());
                showTemp = jsonObject.getString("temp");
                Log.e("Celsius", showTemp);

                /** Second Clouds picture **/
                weather = response.getString("weather");
                Log.e("ArrayListWeather", weather);

                JSONArray jsonclouds = new JSONArray(weather.toString());

                ArrayList<String> list = new ArrayList<String>();

                for (int i = 0; i < jsonclouds.length(); i++) {

                    list.add(jsonclouds.get(i).toString());
                }

                Log.e("List: ", list.get(0));

                JSONObject cloudspict = new JSONObject(list.get(0).toString());
                Log.e("cloudspict", String.valueOf(cloudspict));

                JSONObject jsonObject1 = new JSONObject(cloudspict.toString());
                clouds = jsonObject1.getString("icon");
                Log.e("Clouds is ", clouds);


                try {
                    /** temperature **/
                    celsius = Double.parseDouble(showTemp);
                    celsius = celsius - KelvinToCels;

                    String andTemp = String.format("%.1f", celsius);
                    giveTemp.setText(Temp + andTemp + " C");

                    /** picture **/
                    Picasso.with(Weather.this).load("http://openweathermap.org/img/w/" + clouds + ".png").into(cloudImage);

                    /** Stop spinner **/
                    spinner.setVisibility(View.GONE);

                } catch (NumberFormatException e) {
                    Log.e("Something went wrong", e.toString());
                    e.printStackTrace();

                }
            } catch (JSONException ex) {
                Log.e("App", "Failure", ex);
            }
        }

    }

//
//    private class RequestTask extends AsyncTask<String, Void, JSONObject> {
//
//        @Override
//        protected void onPreExecute() {
//            spinner.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected JSONObject doInBackground(String... city) {
//            String str = "http://api.openweathermap.org/data/2.5/weather?q=" + city[0] + "&APPID=65dbec3aae5e5bf9000c7a956c8b76f6";
//            URLConnection urlConn = null;
//            BufferedReader bufferedReader = null;
//            try {
//                URL url = new URL(str);
//                urlConn = url.openConnection();
//                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
//
//                StringBuffer stringBuffer = new StringBuffer();
//                String line;
//                while ((line = bufferedReader.readLine()) != null) {
//                    stringBuffer.append(line);
//                    System.out.println("Line is: " + line);
//
//                }
//
//                return new JSONObject(stringBuffer.toString());
//            } catch (Exception ex) {
//                Log.e("App", "yourDataTask", ex);
//                return null;
//            } finally {
//                if (bufferedReader != null) {
//                    try {
//                        bufferedReader.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }

//        @Override
//        protected void onPostExecute(JSONObject response) {

//            if (response != null) {
//                String temperature;
//                String showTemp;
//                String Search = "main";
//                String clouds;
//                String weather;
//
//                /** 0 celsius = -273,15 **/
//                double KelvinToCels = 273.15;
//                double celsius;
//
//                try {
//                    /** First the temperature **/
//                    Log.e("App", "Success: " + response.getString(Search));
//                    temperature = response.getString(Search);
//
//                    JSONObject jsonObject = new JSONObject(temperature.toString());
//                    showTemp = jsonObject.getString("temp");
//                    Log.e("Celsius", showTemp);
//
//                    /** Second Clouds picture **/
//                    weather = response.getString("weather");
//                    Log.e("ArrayListWeather", weather);
//
//                    JSONArray jsonclouds = new JSONArray(weather.toString());
//
//                    ArrayList<String> list = new ArrayList<String>();
//
//                    for (int i = 0; i < jsonclouds.length(); i++) {
//
//                        list.add(jsonclouds.get(i).toString());
//                    }
//
//                    Log.e("List: ", list.get(0));
//
//                    JSONObject cloudspict = new JSONObject(list.get(0).toString());
//                    Log.e("cloudspict", String.valueOf(cloudspict));
//
//                    JSONObject jsonObject1 = new JSONObject(cloudspict.toString());
//                    clouds = jsonObject1.getString("icon");
//                    Log.e("Clouds is ", clouds);
//
//
//                    try {
//                        /** temperature **/
//                        celsius = Double.parseDouble(showTemp);
//                        celsius = celsius - KelvinToCels;
//
////                        giveTemp = (TextView) findViewById(R.id.textView);
//                        String andTemp = String.format("%.1f", celsius);
//                        giveTemp.setText("Temp: " + andTemp + " C");
//
//                        /** picture **/
//                        cloudImage = (ImageView) findViewById(R.id.imageView);
//                        Picasso.with(Weather.this).load("http://openweathermap.org/img/w/" + clouds + ".png").into(cloudImage);
//
//                        /** Stop spinner **/
//                        spinner.setVisibility(View.GONE);
//
//                    } catch (NumberFormatException e) {
//                        Log.e("Something went wrong", e.toString());
//                        e.printStackTrace();
//
//                    }
//                } catch (JSONException ex) {
//                    Log.e("App", "Failure", ex);
//                }
//            }
        }



