package com.example.luitzen_janbeiboer.finalapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Locale;

@EActivity(R.layout.activity_details)
public class Details extends AppCompatActivity {


    @ViewById
    TextView nameLunch;
    @ViewById
    TextView nameMeal;
    @ViewById
    TextView diets;

    // JSON Node names
    private static final String TAG_NAMELUNCH = "lunchMenu";
    private static final String TAG_NAMEMEAL = "nameMeal";
    private static final String TAG_DIETS = "diets";


    @ViewById
    ListView dietListView;

    @AfterViews
    protected void setupViews() {

        /** set language device **/
        Log.e("Language", Locale.getDefault().getDisplayLanguage());
        String Language = Locale.getDefault().getDisplayLanguage();


        ArrayList<String> theList = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, theList);

        // getting intent data
        Intent in = getIntent();
        String name = in.getStringExtra(TAG_NAMELUNCH);
        String cost = in.getStringExtra(TAG_NAMEMEAL);
        String description = in.getStringExtra(TAG_DIETS);

        nameLunch.setText(name);
        nameMeal.setText(cost);
        diets.setText(description);

        /** the RequireDietFilters **/
        ArrayList<String> dietName = in.getStringArrayListExtra("dietListName");
        ArrayList<String> dietLetter = in.getStringArrayListExtra("dietListLetter");

        /** set text into the correct language **/
        String firstWord;
        if (Language.equals("suomi")) {
            firstWord = "\nRuokavalio kirjain merkityksellä";
        } else {
            firstWord = "\nThe diets letter with meaning";
        }
        theList.add(firstWord);

        for (int i = 0; i < dietName.size(); i++) {
            String textOutput;

            if (Language.equals("suomi")) {
                textOutput = "Nimi: " + dietName.get(i) + "\n" + "Kirje: " + dietLetter.get(i);
            } else {
                textOutput = "The Name: " + dietName.get(i) + "\n" + "The Letter: " + dietLetter.get(i);
            }
            theList.add(textOutput);
        }
        dietListView.setAdapter(adapter);
    }

}
